$(function() {

    function scoutsScrollTop() {
		var $totop = $('footer .totop');
		var $body = $('html, body');
			$totop.on('click', function(e){
				e.preventDefault();
				$body.animate({scrollTop:0}, 500, 'swing')
		})
	}
	scoutsScrollTop();

	var d = document.getElementsByClassName(".owl-nav");
	d.className += " otherclass";




	loader();
	/*====================================
	*     LOADER
	======================================*/
	function loader(_success) {
		var obj = document.querySelector('.preloader_script'),
			inner = document.querySelector('.preloader_script_inner'),
			page = document.querySelector('.wrapper');
		obj.classList.add('show');
		page.classList.remove('show');
		var w = 0,
			t = setInterval(function () {
				w = w + 1;
				inner.textContent = w + '%';
				if (w === 100) {
					obj.classList.remove('show');
					page.classList.add('show');
					clearInterval(t);
					w = 0;
					if (_success) {
						return _success();
					}
				}
			}, 20);
	}


}); 


$(window).load(function() {

});
