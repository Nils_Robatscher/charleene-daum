$('.owl-carousel').owlCarousel({
    center: true,
    loop: true,
    nav: true,
    lazyLoad: true,
    navText: [
        "<i class='fa fa-chevron-left'></i>",
        "<i class='fa fa-chevron-right'></i>"
    ]
});


