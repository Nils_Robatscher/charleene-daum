<?php


// Our custom post type function
function create_posttype() {

    register_post_type( 'Referenzen',
    // CPT Options
        array(
            'labels' => array(
                'name' => __('Referenzen' ),
                'singular_name' => __('Referenzen' )
            ),
            'public'        => true,
            'has_archive'   => true,
            'rewrite'       => array('slug' => ''),
            'taxonomies'    => array('referenzen'),
            'supports'      => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments'),
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );

add_theme_support('post-thumbnails');

// Register Custom Taxonomy
function referenzen_tax() {

    $args = array(
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'supports'                   => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments'),
    );
    register_taxonomy( 'referenzen', array('Referenzen' ), $args );

}
// Hook into the 'init' action
add_action( 'init', 'referenzen_tax', 0 );
