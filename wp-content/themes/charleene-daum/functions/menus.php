<?php

function owa_menus() {
  register_nav_menus(
    array(
    'header-menu' => __( 'Header Menü' ),
    'header-top-menu' => __( 'Header Top Menü' ),
    'footer-menu' => __( 'Footer Menü' )
    )
  );
}
add_action( 'init', 'owa_menus' );

 ?>
