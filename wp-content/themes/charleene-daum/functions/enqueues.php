<?php

function enqueue_styles(){
  wp_enqueue_style("style", get_template_directory_uri() . "/dist/css/style.css?v=".time(), [], false, 'all');
}

function enqueue_scripts(){
  wp_deregister_script("jquery");
  wp_enqueue_script("jquery", get_template_directory_uri() . "/js/vendor/jquery.min.js", [], '', true);
  #wp_enqueue_script("bootstrap", get_template_directory_uri() . "/js/vendor/bootstrap.js", ["jquery"], '', true);
  wp_enqueue_script("owl", get_template_directory_uri() . "/js/vendor/owl.carousel.min.js", ["jquery"], '', true);
  wp_enqueue_script("slider", get_template_directory_uri() . "/js/slider.js?v=".time(), ["jquery"], '', true);
  wp_enqueue_script("filter", get_template_directory_uri() . "/js/filter.js?v=".time(), ["jquery"], '', true);
  wp_enqueue_script("app", get_template_directory_uri() . "/js/app.js?v=".time(), ["jquery"], '', true);
}

if(!is_admin()){
  //is frontend
  add_action("wp_enqueue_scripts", "enqueue_styles");
  add_action("wp_enqueue_scripts", "enqueue_scripts");
}

//remove Open Sans
if (!function_exists('remove_wp_open_sans')) :
  function remove_wp_open_sans() {
    wp_deregister_style( 'open-sans' );
    wp_register_style( 'open-sans', false );
  }
  add_action('wp_enqueue_scripts', 'remove_wp_open_sans');
endif;

?>
