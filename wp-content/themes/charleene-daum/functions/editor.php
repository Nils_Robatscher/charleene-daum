<?php

// disable gutenberg for posts
add_filter('use_block_editor_for_post', '__return_false', 10);
// disable gutenberg for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);

add_action( 'admin_init', 'add_my_editor_style' );

function add_my_editor_style() {
    add_editor_style( '/dist/editor-style.css' );
}

 ?>
