<?php

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Localisation Support
    load_theme_textdomain('scouts', get_template_directory() . '/languages');
}


// disable gutenberg for posts
add_filter('use_block_editor_for_post', '__return_false', 10);

// disable gutenberg for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);
define( 'WP_DEBUG', true );
 ?>
