<?php

add_theme_support('post-thumbnails');

add_image_size('slider', 920, 500, true); // Partner Thumbnail
add_image_size('galerie', 500, 500, true); // Medium Thumbnail
add_image_size('header', 1500, 800, true);
add_image_size('hochkant', 700, 1400, true);

//Allow SVG through WordPress Media Uploader
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

?>
