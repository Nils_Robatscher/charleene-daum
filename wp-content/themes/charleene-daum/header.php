<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <div class="preloader_script">
        <div class="preloader_script_inner">100%</div>
    </div>
    <div class="wrapper">
        <header>
            <div class="container">
                <div class="logo">
                    awd
                </div>
                <nav>
                    <?php wp_nav_menu(array('theme_location' => 'header-menu')); ?>
                </nav>
            </div>
        </header>

    <div class="content-area">
    <div class="container p-0">
