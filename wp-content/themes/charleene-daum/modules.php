<?php
  if(have_rows('modules')){
    while(have_rows('modules')){
      the_row();
      switch(get_row_layout()){
        case 'headline':
          get_template_part('modules/headline');
          break;
          case 'subline':
          get_template_part('modules/subline');
          break;
          case 'copytext':
          get_template_part('modules/copytext');
          break;
          case 'galerie':
          get_template_part('modules/galerie');
          break;
          case 'contact_form':
          get_template_part('modules/contact_form');
          break;
          case 'headerbild':
          get_template_part('modules/headerbild');
          break;
          case 'galerie_responsiv':
          get_template_part('modules/galerie_responsiv');
          break;
          case 'slider_carousel':
          get_template_part('modules/slider_carousel');
          break;
          default:
          break;
      }
    }
  }
?>
