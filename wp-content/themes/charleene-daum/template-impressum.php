<?php  /* Template Name: Impressum */ get_header(); ?>

<div class="content">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php get_template_part('modules'); ?>

<?php endwhile;
endif; ?>
</div>

<?php get_footer(); ?>