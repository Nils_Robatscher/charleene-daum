<?php get_header(); ?>

<div class="page-header e404 text-center">
	<h1>404</h1>
	<h2 class="subline"><?php _e( 'Diese Seite konnte leider nicht gefunden werden', 'wp_scouts' ) ?></h2>
	<div class="row content">
		<div class="col-sm-12">
			<a href="<?php echo get_site_url(); ?>" class="button"><?php _e( 'Zurück zur Startseite', 'wp_scouts' ) ?></a>
		</div>
	</div>

</div>

<?php get_footer(); ?>
