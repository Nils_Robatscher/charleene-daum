<section class="imagegalerie">


    <?php
    $images = get_sub_field('galerie');
    if( $images ): ?>
    <div class="container">
        <ul class="row p-0">
            <?php foreach( $images as $image ): ?>
                <li class="col-sm-4 column">
                    <a href="<?php echo esc_url($image['url']); ?>">
                        <img class="<?php echo esc_html($image['caption']); ?>" id=" filterDiv  <?php echo esc_html($image['caption']); ?> " src="<?php echo esc_url($image['sizes']['galerie']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    </a>
                    <p><?php echo esc_html($image['caption']); ?></p>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php endif; ?>
</section>