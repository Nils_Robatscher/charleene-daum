<section class="copytext">
    <div class="container">
        <div class="col-7 copytext-content">
            <?php the_sub_field('copytext'); ?>
        </div>
    </div>
</section>
