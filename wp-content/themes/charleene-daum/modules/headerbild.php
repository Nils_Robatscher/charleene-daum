<div class="headerbild">
    <div class="container p-0">
        <?php
        $image = get_sub_field('headerbild');
        if( !empty( $image ) ): ?>
            <img src="<?php echo esc_url($image['sizes']['header']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
        <?php endif; ?>
    </div>
</div>
