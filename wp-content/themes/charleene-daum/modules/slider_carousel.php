<section class="carousel">
            <?php
    $images = get_sub_field('slider_carousel');
    if( $images ): ?>
    <div class="container ">
        <ul class="owl-carousel ">
            <?php foreach( $images as $image ): ?>
                <li class="">
                    <!--<a href="<?php echo esc_url($image['url']); ?>">-->
                        <img id=" filterDiv  <?php echo esc_html($image['caption']); ?> " src="<?php echo esc_url($image['sizes']['slider']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                     <!--</a>-->
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    </div>
</section>
