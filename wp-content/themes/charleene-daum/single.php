<?php get_header(); ?>
<section class="blog">
    <div class="container">
        <?php if ( has_post_thumbnail() ) { ?>
            <div class="headerimg">
                <?php the_post_thumbnail( 'header_flex' ); ?>
            </div>
        <?php } ?>
        <h1><?php the_title(); ?></h1>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php the_content() ?>
        <?php endwhile;
        endif; ?>
    </div>
</section>
<?php get_footer(); ?>
