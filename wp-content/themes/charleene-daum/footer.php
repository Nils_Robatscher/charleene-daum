  <?php // end: div.content-area ?>
  </div>

  <!-- footer -->
  <footer class="footer" role="contentinfo">
    <div class="container">
      <div class="copyright">
        Alle Photos © Charlene D, Daum
      </div>
      <div class="nav">
        <?php wp_nav_menu(array('theme_location' => 'footer-menu')); ?>
      </div>
    </div>
  </footer>
  <!-- /footer -->

  <?php // end: div.wrapper ?>
  </div>
</div>
  <?php wp_footer(); ?>
  </body>
</html>
